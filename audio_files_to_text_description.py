#! /usr/bin/env nix-shell
#! nix-shell -i python -p "python3.withPackages (ps: with ps; [ mutagen ])"
"""
Tool to keep only a textual description for a directory of audio files
Copyright (C) 2022  Thibault Lemaire <thibault.lemaire@protonmail.com>
SPDX-Licence-Identifier: AGPL-3.0-or-later

Inspired from https://github.com/quodlibet/mutagen/blob/941585251a34b0d46e8033e7535b39b611dbb0ce/mutagen/_tools/mutagen_inspect.py
"""

import argparse
from pathlib import Path
import sys

from mutagen import File

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('source', help="input directory of audio files")
    parser.add_argument('target', help="output target directory of file descriptions")
    args = parser.parse_args()

    src = Path(args.source)
    assert src.exists() and src.is_dir()
    tgt = Path(args.target)
    assert tgt.exists() and tgt.is_dir()

    for entry in src.iterdir():
        if not entry.is_file():
            continue

        try:
            info = File(entry)
        except AttributeError:
            print("Not an audio file: ", entry.path, file=sys.stderr)
            continue
        
        desc_file_name = entry.name + ".txt"
        desc_file = tgt / desc_file_name
        desc_file.write_text(info.pprint())

if __name__ == "__main__":
    main()
