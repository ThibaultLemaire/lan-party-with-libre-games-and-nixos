# A Live NixOS system for LAN Gaming

This repository gives you the tools to let participants boot into a live system that automatically downloads the games from the LAN Host on first launch.

That means they don't need to install the games (everything lives in RAM and will be gone the moment they poweroff their machine), and don't need any internet connection at all.

For context on why I made this, see [my blog post](https://thibaultlemaire.gitlab.io/blog/hosting-a-lan-party-with-libre-games-and-nixos/).

Here is a high-level overview of how that works:

![overview](overview-schema.svg)

> Schema made with [Excalidraw](excalidraw.com) all assets used are MIT, except for the Nix flake logo which I derived from an original work of Tim Cuthbertson under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.en)

(Other types of participants and the Internet connection are included for illustration, but let's focus on the participants that will be using the live system.)

If the guest machine has an RJ45 port, it can connect to the switch and netboot the live system directly from the Host. When the live system is up, the participant can click on the game they want to play, and it will be downloaded from the host.

Technical explanation:
1. You have to go to the BIOS/UEFI settings of the guest machine to configure it to boot over PXE.
(Depending on the hardware, this is the hard and sometimes dangerous part, as editing BIOS settings is made unecessarily complicated by some vendors.)
2. The machine requests a PXE server from the router via DHCP, and the router gives it the address of the LAN Host.
3. The LAN Host then serves it a minimal bootstrap, which will download the full squashfs live image over iPXE/TFTP.
(PXE is extremely slow, after bootstrapping to iPXE the only limit is the 1Gb/s rate of the router.
In my tests, it took less than 30s to transfer the image from host to guest, half of it being the PXE->iPXE bootstrap.
Or 1min if the host is connected to the router over wifi.)
4. The guest proceeds to boot into the live image
5. The participant clicks on a game.
6. Nix (the NixOS package manager) checks if it has the game locally, and if so, skips to 8.
7. Nix tries to download the game from its list of binary caches in order.
The live image being configured with the host as its first binary cache, this is where it will reach out first.
(This is how you can avoid the need for Internet connectivity completely.
Also note that if the participant tries to install/launch any software that is not installed on the host, it will fallback to downloading it from the official NixOS caches.)
8. The game launches

If the guest machine does __not__ have an RJ45 port, it can boot from a USB drive loaded with the same live image and connect to wifi to download the games, so it works essentially the same.
(The live image should be preconfigured with the wifi credentials, so everything just works without human intervention.
Which also has the added benefit that participants that were previously wired, can simply unplug when they have the game loaded and seemlessly switch to wifi to free up a port on the switch.)

If the machine has enough RAM (more than 2GiB), you can even choose "Load to RAM" when booting in order to remove the USB drive when you've reached the graphical interface and let someone else use the drive.

Here is a screenshot of the guest system user interface:

![Screenshot of the guest system](./Screenshot_20221231_165946.png)

## I want to host a similar event, and reuse what you made here

This is why I chose NixOS. I'm sure the same results can be achieved with other linux distros, but

1. Nix is what I know
2. I can share my system configuration as a handful of descriptive files that are guaranteed to produce the exact same result.
3. It was fun to make

I have tried to comment the configuration files exhaustively, so, provided you have a basic understanding of how [NixOS](nixos.org) works, you should just be able to follow along the code.

[__👉 You should start by looking at `etc/nixos/lan-party.base.nix`. 👈__](etc/nixos/lan-party.base.nix)

## Music

For this event I had also prepared a list of tracks to play to set the vibe, and of course all of them are libre/free culture! (Meaning at least CC-BY-NC-ND)

My small selection takes 707.9 MiB of disk space, and you can freely obtain them from diverse sources, so I only included their metadata in this repository.


__You can find all the tracks in the [`./music/`](./music/) folder.__


Broadly speaking they are mostly Electro, but specifically, there is some Goa, Trance, Techno, Dubstep, and Synthwave.
(That's what _l337 g4m3rzzz_ listen to right? No? Well, that's certainly what I want to listen to when I play!)

Most come from [Jamendo](jamendo.com), some from Wikimedia Commons, some from the Free Music Archive, others from the game [Xonotic](xonotic.org) itself, even! And a few from Soundcloud, surprisingly.

You should have enough metadata to find the songs by yourself, I'm sorry I don't have a direct link to all of them yet.
(Proper crediting takes time, and for now I prefer to focus on the NixOS part of things.
Feel free to open an issue or shoot me an email to motivate me if you can't find a song, or you'd like more info!)

## List of Games

- [Teeworlds](https://teeworlds.com/) v0.7.5  
  ``` #Shooter #Arcade #2D```
- [Minetest](https://www.minetest.net/) v5.6.1  
  ```#Sandbox #FirstPerson #Building #3D```
- [Mindustry](https://mindustrygame.github.io/) v126.2  
  ```#TowerDefense #Management #Arcade #2D #SciFi```
- [Hedgewars](http://www.hedgewars.org/) v1.0.2  
  ```#Strategy #Shooter #TurnBased #2D```
- [The Battle for Wesnoth](http://www.wesnoth.org/) v1.16.5  
  ```#Strategy #TurnBased #2D #Fantasy```
- [0 A.D.](https://play0ad.com/) v0.0.26  
  ```#Strategy #Management #3D #Historic```
- [Super Tux Kart](https://supertuxkart.net) v1.3  
  ```#Racing #Arcade #3D```
- [Stunt Rally](http://stuntrally.tuxfamily.org/) v2.6.2  
  ```#Racing #Realistic #3D```
- [Xonotic](https://xonotic.org/) v0.8.5  
  ```#Shooter #FirstPerson #Arena #3D #SciFi```
- [Red Eclipse](https://www.redeclipse.net/) v2.0.0  
  ```#Shooter #FirstPerson #Arena #Parkour #3D #SciFi```
