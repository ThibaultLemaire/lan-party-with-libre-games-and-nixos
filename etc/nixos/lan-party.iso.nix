# Same as ./lan-party.pixiecore.nix but to build a bootable ISO image
# It is possible that even more code could be shared between those 2 files, but
# this was practical enough for my purposes, and pushing further proved difficult
{nixpkgs, ...}:

import "${nixpkgs}/nixos" {
  system = "x86_64-linux";

  configuration = { config, pkgs, lib, ... }: with lib; {
    imports = [

      ./lan-party.base.nix

      # We want to turn this configuration into a bootable ISO image
      "${nixpkgs}/nixos/modules/installer/cd-dvd/iso-image.nix"
      # Pack as many drivers as we can
      "${nixpkgs}/nixos/modules/profiles/all-hardware.nix"
      # The minimal profile disables a bunch of stuff to reduce the final image
      # size, but we re-enable Xlibs, SEE ./lan-party.base.nix[7]
      "${nixpkgs}/nixos/modules/profiles/minimal.nix"
    ];

    # Preload the nixpkgs source, to accelerate initial resolution of packages
    # by `nix run`
    config.nix = {
      registry.nixpkgs.flake = nixpkgs;
    };

    config.isoImage = {
      makeEfiBootable = true;
      makeUsbBootable = true;
    };
  };
}
