# Builds upon ./lan-party.base.nix to make all the parts required for
# a netbootable system
{nixpkgs, ...}:

import "${nixpkgs}/nixos" {
  system = "x86_64-linux";

  configuration = { config, pkgs, lib, ... }: with lib; {
    imports = [

      ./lan-party.base.nix

      # We want to turn this configuration into a netboot-able squashfs
      "${nixpkgs}/nixos/modules/installer/netboot/netboot.nix"
      # Pack as many drivers as we can
      "${nixpkgs}/nixos/modules/profiles/all-hardware.nix"
      # The minimal profile disables a bunch of stuff to reduce the final image
      # size, but we re-enable Xlibs, SEE ./lan-party.base.nix[7]
      "${nixpkgs}/nixos/modules/profiles/minimal.nix"
    ];

    # Preload the nixpkgs source, to accelerate initial resolution of packages
    # by `nix run`
    nix = {
      registry.nixpkgs.flake = nixpkgs;
    };
  };
}
