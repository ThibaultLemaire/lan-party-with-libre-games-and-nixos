# This is your typical system configuration.nix
# I have elided everything unrelated to the LAN Party, so don't expect it to
# work as-is, you'll have to merge it with a working system configuration
#
# It describes what you'll need on the host system for everything to work
#
# JUMP to [4]
{
  config,
  pkgs,
  lib,
  nixpkgs-stable,
  ... }@attrs:

let
  lan-net-boot = import ./lan-party.pixiecore.nix attrs;
  lan-iso = import ./lan-party.iso.nix attrs;
  lan-build-results = pkgs.runCommand "lan-party-iso" # [7]
    {
      # SEE ./lan-party.iso.nix
      # The /share/ directory is one of the special directories in derivations
      # that get linked to the current-system when installed as part of the
      # environment.systemPackages
      iso = "${lan-iso.config.system.build.isoImage}/iso/nixos.iso";
      # The netboot ramdisk is linked here just so I could measure its size
      # when I was building the live system and trying out different options
      ramdisk = "${lan-net-boot.config.system.build.netbootRamdisk}/initrd";
    }
    ''
      target=$out/share/$name
      mkdir -p $target
      ln -s $iso $target
      ln -s $ramdisk $target
    '';
in
{
  services = {
    # [4] Pixiecore is an all-in-one solution for netbooting. It sets up a PXE
    # server that bootstraps to iPXE to send the kernel and initial ramdisk
    pixiecore = with lan-net-boot.config.system.build; {
      enable = true;
      dhcpNoBind = true;
      openFirewall = true; # Not enough SEE [5]
      port = 64172;
      statusPort = 64172; # SEE [6]
      # The netbootRamdisk and kernel will be built as part of the host system
      # based on the configuration in ./lan-party.base.nix.
      # SEE ./lan-party.pixiecore.nix
      initrd = "${netbootRamdisk}/initrd";
      kernel = "${kernel}/bzImage";
      cmdLine = "init=${toplevel}/init loglevel=4";
    };

    # Here is where the Nix binary cache is setup. This is how the host can
    # serve the packages it has in its local nix store to the guest systems.
    nix-serve = {
      enable = true;
      # This is where you would setup certificates and package signing keys in
      # a professional scenario
    };
    nginx = {
      enable = true;
      virtualHosts = {
        "host" = {
          serverAliases = [ "binarycache" ];
          locations."/".extraConfig = ''
            proxy_pass http://localhost:${toString config.services.nix-serve.port};
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
          '';
        };
      };
    };
  };

  environment = {
    systemPackages = with pkgs; [
      # Build and install the ISO image to
      # /run/current-system/sw/share/lan-party-iso/nixos.iso
      # so you can then flash it to a USB drive. SEE [7]
      lan-build-results

      # [1] All the available games that must be cached on the host machine
      minetest
      stuntrally
      xonotic
      wesnoth
      teeworlds
      hedgewars
      superTuxKart
      zeroad
      redeclipse
      mindustry
      # Other miscellaneous programs referenced in ./lan-party.base.nix
      cool-retro-term
      htop
    ];
  };

  # And that's all you need for hosting a netboot server that doubles as a local
  # binary cache for on-the-fly downloading of games.

  networking = {
    # [3] You have to open the appropriate ports for each game to be able to
    # host a local server. Although, technically, you could get away with just a
    # couple of ports opened, and configure each game to use those ports.
    # Except for pixiecore, I didn't take the time to test if both UDP & TCP
    # were required to be open.
    firewall = {
      allowedUDPPorts = [
        # Minetest
        30000
        # SuperTuxKart
        2757 2759
        # teeworlds
        8303
        # wesnoth
        15000
        # 0ad
        20595
        # Mindustry
        6567
        # [5] pixiecore
        67 69 4011
        # Red Eclipse
        28800 28801 28802 28888
        # Xonotic
        26000
        # Hedgewars
        46631
      ];
      allowedTCPPorts = [
        80 # Nginx
        # Minetest
        30000
        # SuperTuxKart
        2757 2759
        # teeworlds
        8303
        # wesnoth
        15000
        # 0ad
        20595
        # Mindustry
        6567
        # [6] pixiecore
        64172
        # Red Eclipse
        28800 28801 28802 28888
        # Hedgewars
        46631
      ];
    };
  };

  programs.firefox.enable = true; # [2]

  # So a few days before the event, I decided to update nixpkgs because
  # Hedgewars which was previously broken had been fixed. Except that it was now
  # pixiecore that was broken!
  # But this gives us an opportunity to see the power of Nix in action:
  # Just grab a version of pixiecore from when it was working!
  # SEE ./flake.nix[1]
  nixpkgs = {
    overlays = [
      (self: super: {
        pixiecore = nixpkgs-stable.legacyPackages.x86_64-linux.pixiecore;
      })
    ];
  };
  # In case you're wondering, the opposite (grabbing the fixed Hedgewars version)
  # would have complicated resolution of the appropriate derivation on the guest
  # systems

  # Required for `nix run` and friends
  nix.settings.experimental-features = [ "nix-command" "flakes" ];

}

