# Like a configuration.nix, this describes a full NixOS system.
#
# This is the system that will be live booted by the participants and used to
# download and play games, so we want it light enough to be loaded in RAM, but
# feature-complete enough to be used by people unfamiliar with Linux.
#
# Advanced features like removing a game to free RAM are provided, but it's OK
# if they require the host (or someone savvy enough) to help.
#
# Also, never underestimate people, if you make it so they can't get it wrong,
# once they know something can be done, they'll figure it back out.
#
# In this guided explanation, I will be using the keywords JUMP, SKIP, and SEE,
# to give you indications on where to focus your attention next.
#  - JUMP means to simply go to the indicated anchor and keep reading on from
#    there. (Don't come back.)
#  - SKIP works like JUMP, but is optional. You won't be getting back to the
#    skipped sections.
#  - SEE is optional like SKIP, but means you should come back to where you were
#    after reading the indicated paragraph.
#
# JUMP to [1]

{ config, pkgs, lib, ... }:

let
  username = "player1"; # [5]

  selenux-wm-themes = pkgs.fetchFromGitHub { # [4]
    owner = "ItzSelenux";
    repo = "selenux-wm-themes";
    rev = "0e815773c620a5ec7160af253657f2ae23410831";
    sha256 = "0kidxOZuzcbwkbWa0rH3oKTfLo4Te6ntSALOAiln5hY=";
  };
in

with lib;

{
  # [1] The system does not come preloaded with any game, just a bunch of
  # shortcuts that will be listed in the applications menu.
  # Those shortcuts all call a simple script with different arguments.
  #
  # In essence, this script is just calling `nix run $game` and nix figures the
  # rest on its own. If you are familiar with `cargo run` or `npm run`, this is
  # in essence the same thing: nix downloads/builds the target package (called
  # 'derivations') and all its dependencies and puts it in the nix store, then
  # starts it.
  #
  # The added complexity in that script is just to make it easier to remove only
  # a selection of games, to free up RAM while keeping the interesting games in
  # cache. If you are not interested in how that works, SKIP to [2]
  # 
  # "(Un)installing" software with nix is a somewhat meaningless notion.
  # Nix puts everything in /nix/store and never deletes anything unless you
  # specifically ask it to. This is called garbage-collecting, and removes all
  # the derivations that are not used by your system.
  #
  # How it determines what is or isn't "used" is the key here.
  # It all boils down to the notion of store root. A derivation that is
  # explicitely pinned to survive garbage-collect sweeps. (Its dependency graph
  # is also spared.)
  #
  # In a common NixOS system, the final derivation describing your system is
  # automatically pinned (the result of evaluating your configuration.nix), but
  # you can also manually pin derivations.
  #
  # This script places a pin on the games that are downloaded and puts it the
  # home folder. If you are running out of RAM you can unpin the games you want
  # to delete with `rm ~/$game`, then run `nix-collect-garbage` or the easier
  # to remember abbreviation I added: `clean`. SEE [3]
  #
  # `nix run` is unable to figure out the binary to run for some of those
  # derivations, so there's also a bit of tooling with a third argument to the
  # script (`argv[2]`) to fix that.
  environment.systemPackages =
    let
      launch-game = pkgs.writeScript "launch-game.fish" ''
        #!/usr/bin/env fish

        set name $argv[1]
        set derivation nixpkgs#$name
        set pin ~/$name
        if not test -e $pin
          nix-store --add-root $pin --realise (nix eval $derivation.outPath | tr -d '\"')
        end
        if set -q argv[2]
          nix shell $derivation -c $argv[2]
        else
          nix run $derivation
        end
      '';
      make-shortcut =
        { name,
          exec ? "",
          ...
        }@args:
        pkgs.makeDesktopItem ( # Creates a .desktop shortcut
          { categories = [ "Game" ]; }
          // args
          // { exec = "${launch-game} ${name} ${exec}"; }
        );
    in [
    # [2] Note that the corresponding derivations should be installed on the
    # host system, otherwise they will be downloaded from the Internet.
    # SEE ./configuration.nix[1]
    (make-shortcut {
      name = "teeworlds";
      desktopName = "Teeworlds";
    })
    (make-shortcut {
      name = "hedgewars";
      desktopName = "Hedgewars";
    })
    (make-shortcut {
      name = "mindustry";
      desktopName = "Mindustry";
    })
    (make-shortcut {
      name = "minetest";
      desktopName = "Minetest";
    })
    (make-shortcut {
      name = "xonotic";
      desktopName = "Xonotic";
    })
    (make-shortcut {
      name = "stuntrally";
      desktopName = "Stunt Rally";
      exec = "stuntrally";
    })
    (make-shortcut {
      name = "zeroad";
      desktopName = "0 A.D.";
      exec = "0ad";
    })
    (make-shortcut {
      name = "superTuxKart";
      desktopName = "Super Tux Kart";
    })
    (make-shortcut {
      name = "redeclipse";
      desktopName = "Red Eclipse";
    })
    (make-shortcut {
      name = "wesnoth";
      desktopName = "The Battle for Wesnoth";
    })
    (make-shortcut { # SEE ./configuration.nix[2]
      name = "firefox";
      desktopName = "Firefox";
      categories = [ "WebBrowser" ];
    })
  ];

  # This is where the magic happens.
  # We are configuring nix (the package manager) to fetch software binaries from
  # the host machine first, then from the official cache if that fails
  nix.settings = {
    substituters = [
      "http://192.168.1.135" # 🔧 Change it with your host machine IP address
      # Or, with the appropriate DNS config on your router, you can put
      # something fancy like:
      # "http://host.lan" 

      "https://cache.nixos.org/" # The official repo as fallback
    ];
    # Disable signature checking because it's easier that way.
    require-sigs = false; # ⚠️ Don't do this at work kids
    # It's a one off event, I trust the cache server (it's my machine), and
    # this is a live system anyway, there isn't much to gain for an attacker.
    # In a serious scenario, you would set up signing keys on the host and
    # trust them here.
  };

  networking = {
    wireless = {
      enable = true;
      # Wireless connectivity out of the box baby!
      networks = {
        # 🔧 Change the following with your wifi SSID and password
        "LANPARTY" = {
          psk = "letsplay"; # ⚠️ The password will endup as clear text in the 
                            # nix store. Again, I'm trading off security for
                            # ease of configuration.
        };
      };
    };
    hostName = "LAN";
    domain = "party";
    firewall.enable = false; # ⚠️ You cannot host a LAN server for a game with
                             # your firewall up. Disabling the firewall
                             # altogether is overkill, but is the easy way.
                             # Alternatively, you can open ports individually.
                             # SEE ./configuration.nix[3]
  };

  # That wraps it up for the lazy downloading of games on the guest, if that's
  # all you're interested in, you can SKIP to ./configuration.nix.
  
  # OK. We've got the foundations, let's flesh this system up with a graphical
  # interface ✨
  services.xserver = {
    enable = true;
    # After some trial and error, I elected to use icewm. It's a window manager
    # that also doubles as a very basic desktop environment, it's easy enough
    # to theme, and auto-discovers the .desktop files installed above and puts
    # them in the applications menu.
    windowManager.icewm.enable = true;
  };
  nixpkgs.overlays = [
    (self: super: {
      icewm = super.icewm.overrideAttrs (oldAttrs: {
        # The default theme for icewm is white and IMO ugly, so I'm overriding
        # it with a prettier dark theme with orange accents. SEE [4]
        postInstall = ''
          icedir=$out/share/icewm
          cp -r '${selenux-wm-themes}/Graphite/IceGraphite Orange Dark' $icedir/themes/
          echo 'Theme="IceGraphite Orange Dark/default.theme"' > $icedir/theme
        '';
      });
    })
  ];

  # We need to configure a user for this live system
  users.users.${username} = { # SEE [5]
    isNormalUser = true;
    extraGroups = [
      "wheel" # Enable ‘sudo’ for the user.
      "networkmanager"
    ];
    # Let's not bother with a password
    initialHashedPassword = ""; # ⚠️
  };
  security.sudo = {
    enable = true;
    wheelNeedsPassword = false; # ⚠️ 
  };
  # It's okay on a live system, but ofc don't do this at work
  # Note that the user would still need to type `sudo` before any command
  # requiring admin priviledge, they just won't be prompted for a password

  # Let's give them a nice Friendly Interactive SHell.
  users.users.${username}.shell = pkgs.fish;
  # Fish gives you syntax highlighting, auto-completion, suggestions and quick
  # history search out of the box. That's nice for anyone, but all the more for
  # the host when they have to explain what to type to their guests
  programs.fish = {
    enable = true;
    # You can also configure abbreviations that will be expanded in the
    # corresponding string when typed. (They're like aliases, but better since
    # its the full command that appears in history, and you immediately know
    # what they stand for.)
    shellAbbrs = {
      clean = "nix-collect-garbage"; # [3]
      htop = "nix run nixpkgs#htop";
      # The only installed terminal is xterm, which, besides being ugly, cannot
      # be scaled up, making it hard for the host to read on the guest screens.
      # So in the event a terminal is really required, CRT is right at hand.
      crt = "nix run nixpkgs#cool-retro-term";
    };
  };

  # Let's skip the login screen and get the user right into the desktop, ready
  # to play
  services = {
    xserver = {
      displayManager = {
        autoLogin = {
          enable = true;
          user = username;
        };
        lightdm.enable = true;
        lightdm.greeter.enable = false;
      };
    };
    # Same for the virtual ttys (reachable via Ctrl+Alt+{F1-F7}. Useful for
    # troubleshooting when the graphical interface freezes or is otherwise
    # unresponsive.)
    getty.autologinUser = username;
  };

  # On a live system, the entire filesystem lives on RAM. By default tmpfs are
  # configured to only use 50% of the available RAM. On a machine with 4GiB of
  # RAM, that's pretty limiting, and what's not used by the FS can still be used
  # as regular memory, so we can safely crank it up to 90%.
  fileSystems."/nix/.rw-store" = lib.mkForce {
    fsType = "tmpfs";
    options = [
      "mode=0755"
      "size=90%"
    ];
    neededForBoot = true;
  };

  # We're going to be gaming *all night long*, aren't we? Then we'd better take
  # care of our eyes, let's put on a blue light filter
  services.redshift.enable = true;
  # Redshift auto-adjusts the color temperature based on the time of day, but 
  # you need to tell it where you are.
  location = {
    latitude = 45.18;
    longitude = 5.71;
  };

  # The rest is minor tweaking, you can SKIP to ./configuration.nix.

  # The Xanmod Linux Kernel is said to be tuned for desktop and gaming
  # applications (rather than server loads where Linux is usually deployed)
  boot.kernelPackages = pkgs.linuxKernel.packages.linux_xanmod;

  services = {
    xserver = {
      libinput.enable = true; # touchpad support
      displayManager = {
        # Workaround: without this the desktop background doesn't load.
        sessionCommands = ''
          icewmbg
        '';
      };
      layout = "fr"; # [6] French AZERTY keyboard
    };
  };
  console = {
    font = "Lat2-Terminus16";
    useXkbConfig = true; # SEE [6]
  };

  hardware = {
    pulseaudio.enable = true; # Sound support
    # Some hardware I tested did not have the drivers for the wifi card, but
    # that didn't fix it..
    #enableAllFirmware = true;
  };

  environment.noXlibs = false; # [7] Forcing Xlibs to be enabled, otherwise it
                               # would be disabled by the minimal profile
                               # (You need that if you want a graphical interface)

  # Required for `nix run` and friends
  nix.settings.experimental-features = [ "nix-command" "flakes" ];
}
