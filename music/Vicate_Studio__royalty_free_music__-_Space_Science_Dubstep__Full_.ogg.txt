Ogg Vorbis, 167.17 seconds, 112000 bps (audio/vorbis)
ARTIST=Vicate Studio (royalty free music)
TITLE=Space Science Dubstep (Full)
TRACKNUMBER=59
ALBUM=
MUSICBRAINZ_ALBUMARTIST=Vicate Studio (royalty free music)
GENRE=electronica
DATE=2018
JAMENDO-TRACK-ID=1510540
ORGANIZATION=http://www.jamendo.com
COMMENT=http://www.jamendo.com
PUBLISHER=http://www.jamendo.com
DESCRIPTION=http://www.jamendo.com
LICENSE=http://creativecommons.org/licenses/by-nc-sa/3.0/
COPYRIGHT=http://creativecommons.org/licenses/by-nc-sa/3.0/
ENCODED-BY=http://www.jamendo.com
WCOP=http://creativecommons.org/licenses/by-nc-sa/3.0/
WOAF=http://www.jamendo.com/en/track/1510540