Ogg Vorbis, 148.97 seconds, 112000 bps (audio/vorbis)
ALBUM=
TITLE=Hi-Tech Background Dubstep
TRACKNUMBER=45
MUSICBRAINZ_ALBUMARTIST=Vicate Studio (royalty free music)
ARTIST=Vicate Studio (royalty free music)
GENRE=electronic
DATE=2017
JAMENDO-TRACK-ID=1415605
ORGANIZATION=http://www.jamendo.com
COMMENT=http://www.jamendo.com
PUBLISHER=http://www.jamendo.com
DESCRIPTION=http://www.jamendo.com
LICENSE=http://creativecommons.org/licenses/by-nc-sa/3.0/
COPYRIGHT=http://creativecommons.org/licenses/by-nc-sa/3.0/
ENCODED-BY=http://www.jamendo.com
WCOP=http://creativecommons.org/licenses/by-nc-sa/3.0/
WOAF=http://www.jamendo.com/en/track/1415605