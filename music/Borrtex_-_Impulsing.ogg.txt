Ogg Vorbis, 187.00 seconds, 112000 bps (audio/vorbis)
ARTIST=Borrtex
ALBUM=The Impulse
TRACKNUMBER=18
GENRE=filmscore
MUSICBRAINZ_ALBUMARTIST=Borrtex
TITLE=Impulsing
DATE=2018
JAMENDO-TRACK-ID=1582091
ORGANIZATION=http://www.jamendo.com
COMMENT=http://www.jamendo.com
PUBLISHER=http://www.jamendo.com
DESCRIPTION=http://www.jamendo.com
LICENSE=http://creativecommons.org/licenses/by-nc/3.0/
COPYRIGHT=http://creativecommons.org/licenses/by-nc/3.0/
ENCODED-BY=http://www.jamendo.com
WCOP=http://creativecommons.org/licenses/by-nc/3.0/
WOAF=http://www.jamendo.com/en/track/1582091