Ogg Vorbis, 175.52 seconds, 112000 bps (audio/vorbis)
ALBUM=
TITLE=Epic Dubstep (backgrounds)
TRACKNUMBER=16
MUSICBRAINZ_ALBUMARTIST=Vicate Studio (royalty free music)
ARTIST=Vicate Studio (royalty free music)
GENRE=electronic
DATE=2015
JAMENDO-TRACK-ID=1235079
ORGANIZATION=http://www.jamendo.com
COMMENT=http://www.jamendo.com
PUBLISHER=http://www.jamendo.com
DESCRIPTION=http://www.jamendo.com
LICENSE=http://creativecommons.org/licenses/by-nc-nd/3.0/
COPYRIGHT=http://creativecommons.org/licenses/by-nc-nd/3.0/
ENCODED-BY=http://www.jamendo.com
WCOP=http://creativecommons.org/licenses/by-nc-nd/3.0/
WOAF=http://www.jamendo.com/en/track/1235079