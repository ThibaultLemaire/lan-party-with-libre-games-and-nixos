Ogg Vorbis, 208.13 seconds, 112000 bps (audio/vorbis)
MUSICBRAINZ_ALBUMARTIST=DJ Gonalto
TITLE=Trance Adventure
ALBUM=
GENRE=electronic
TRACKNUMBER=3
ARTIST=DJ Gonalto
DATE=2019
JAMENDO-TRACK-ID=1670287
ORGANIZATION=http://www.jamendo.com
COMMENT=http://www.jamendo.com
PUBLISHER=http://www.jamendo.com
DESCRIPTION=http://www.jamendo.com
LICENSE=http://creativecommons.org/licenses/by-nc-nd/3.0/
COPYRIGHT=http://creativecommons.org/licenses/by-nc-nd/3.0/
ENCODED-BY=http://www.jamendo.com
WCOP=http://creativecommons.org/licenses/by-nc-nd/3.0/
WOAF=http://www.jamendo.com/en/track/1670287