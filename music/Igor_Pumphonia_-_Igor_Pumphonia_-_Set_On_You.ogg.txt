Ogg Vorbis, 270.57 seconds, 112000 bps (audio/vorbis)
ALBUM=Set On You
TRACKNUMBER=3
MUSICBRAINZ_ALBUMARTIST=Igor Pumphonia
ARTIST=Igor Pumphonia
TITLE=Igor Pumphonia - Set On You
GENRE=corporate
DATE=2021
JAMENDO-TRACK-ID=1839746
ORGANIZATION=http://www.jamendo.com
COMMENT=http://www.jamendo.com
PUBLISHER=http://www.jamendo.com
DESCRIPTION=http://www.jamendo.com
LICENSE=http://creativecommons.org/licenses/by-nc-nd/3.0/
COPYRIGHT=http://creativecommons.org/licenses/by-nc-nd/3.0/
ENCODED-BY=http://www.jamendo.com
WCOP=http://creativecommons.org/licenses/by-nc-nd/3.0/
WOAF=http://www.jamendo.com/en/track/1839746