Ogg Vorbis, 248.28 seconds, 112000 bps (audio/vorbis)
ALBUM=Time for Love
TITLE=Farewell My Friend
TRACKNUMBER=8
MUSICBRAINZ_ALBUMARTIST=Herr Doktor
ARTIST=Herr Doktor
GENRE=electronic
DATE=2017
JAMENDO-TRACK-ID=1416662
ORGANIZATION=http://www.jamendo.com
COMMENT=http://www.jamendo.com
PUBLISHER=http://www.jamendo.com
DESCRIPTION=http://www.jamendo.com
LICENSE=http://creativecommons.org/licenses/by-nc-sa/3.0/
COPYRIGHT=http://creativecommons.org/licenses/by-nc-sa/3.0/
ENCODED-BY=http://www.jamendo.com
WCOP=http://creativecommons.org/licenses/by-nc-sa/3.0/
WOAF=http://www.jamendo.com/en/track/1416662