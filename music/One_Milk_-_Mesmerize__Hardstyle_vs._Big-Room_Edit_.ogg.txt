Ogg Vorbis, 285.01 seconds, 112000 bps (audio/vorbis)
ARTIST=One Milk
TITLE=Mesmerize (Hardstyle vs. Big-Room Edit)
TRACKNUMBER=15
ALBUM=O1
MUSICBRAINZ_ALBUMARTIST=One Milk
GENRE=electronic
DATE=2017
JAMENDO-TRACK-ID=1499607
ORGANIZATION=http://www.jamendo.com
COMMENT=http://www.jamendo.com
PUBLISHER=http://www.jamendo.com
DESCRIPTION=http://www.jamendo.com
LICENSE=http://creativecommons.org/licenses/by-sa/3.0/
COPYRIGHT=http://creativecommons.org/licenses/by-sa/3.0/
ENCODED-BY=http://www.jamendo.com
WCOP=http://creativecommons.org/licenses/by-sa/3.0/
WOAF=http://www.jamendo.com/en/track/1499607