Ogg Vorbis, 264.00 seconds, 112000 bps (audio/vorbis)
GENRE=dubstep
ARTIST=Kanchi
MUSICBRAINZ_ALBUMARTIST=Kanchi
ALBUM=Backbone
TRACKNUMBER=4
TITLE=Chaos engine
DATE=2012
JAMENDO-TRACK-ID=968256
ORGANIZATION=http://www.jamendo.com
COMMENT=http://www.jamendo.com
PUBLISHER=http://www.jamendo.com
DESCRIPTION=http://www.jamendo.com
LICENSE=http://creativecommons.org/licenses/by-sa/3.0/
COPYRIGHT=http://creativecommons.org/licenses/by-sa/3.0/
ENCODED-BY=http://www.jamendo.com
WCOP=http://creativecommons.org/licenses/by-sa/3.0/
WOAF=http://www.jamendo.com/en/track/968256