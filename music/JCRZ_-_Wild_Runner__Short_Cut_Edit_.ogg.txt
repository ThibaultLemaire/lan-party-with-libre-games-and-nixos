Ogg Vorbis, 277.14 seconds, 112000 bps (audio/vorbis)
GENRE=synthwave
MUSICBRAINZ_ALBUMARTIST=JCRZ
ALBUM=Wild Runner - The Single
TITLE=Wild Runner (Short Cut Edit)
TRACKNUMBER=2
ARTIST=JCRZ
DATE=2017
JAMENDO-TRACK-ID=1505485
ORGANIZATION=http://www.jamendo.com
COMMENT=http://www.jamendo.com
PUBLISHER=http://www.jamendo.com
DESCRIPTION=http://www.jamendo.com
LICENSE=http://creativecommons.org/licenses/by-nc-nd/3.0/
COPYRIGHT=http://creativecommons.org/licenses/by-nc-nd/3.0/
ENCODED-BY=http://www.jamendo.com
WCOP=http://creativecommons.org/licenses/by-nc-nd/3.0/
WOAF=http://www.jamendo.com/en/track/1505485