Ogg Vorbis, 168.00 seconds, 112000 bps (audio/vorbis)
ARTIST=Vicate Studio (royalty free music)
TITLE=Inspiring Adventure Dubstep
TRACKNUMBER=55
ALBUM=
MUSICBRAINZ_ALBUMARTIST=Vicate Studio (royalty free music)
GENRE=electronic
DATE=2017
JAMENDO-TRACK-ID=1484978
ORGANIZATION=http://www.jamendo.com
COMMENT=http://www.jamendo.com
PUBLISHER=http://www.jamendo.com
DESCRIPTION=http://www.jamendo.com
LICENSE=http://creativecommons.org/licenses/by-nc-sa/3.0/
COPYRIGHT=http://creativecommons.org/licenses/by-nc-sa/3.0/
ENCODED-BY=http://www.jamendo.com
WCOP=http://creativecommons.org/licenses/by-nc-sa/3.0/
WOAF=http://www.jamendo.com/en/track/1484978