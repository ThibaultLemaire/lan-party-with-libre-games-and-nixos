Ogg Vorbis, 285.47 seconds, 112000 bps (audio/vorbis)
ARTIST=K4MMERER
TRACKNUMBER=11
GENRE=synthwave
MUSICBRAINZ_ALBUMARTIST=K4MMERER
ALBUM=LONG NIGHTS
TITLE=ARCHADE (INSTRUMENTAL)
DATE=2020
JAMENDO-TRACK-ID=1793126
ORGANIZATION=http://www.jamendo.com
COMMENT=http://www.jamendo.com
PUBLISHER=http://www.jamendo.com
DESCRIPTION=http://www.jamendo.com
LICENSE=http://creativecommons.org/licenses/by-nc-sa/3.0/
COPYRIGHT=http://creativecommons.org/licenses/by-nc-sa/3.0/
ENCODED-BY=http://www.jamendo.com
WCOP=http://creativecommons.org/licenses/by-nc-sa/3.0/
WOAF=http://www.jamendo.com/en/track/1793126